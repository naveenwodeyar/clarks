# Dockerfile - blueprint to to create the dockerImage,
FROM openjdk:21

RUN mkdir /usr/app/

COPY target/Clarks_App.jar /usr/app

WORKDIR /usr/app/

ENTRYPOINT ["java","-jar","Clarks_App.jar"]