package com.series;

public class Fibonacci
{
    static void fiboNacci$eries(int num)
    {
        int a = 0;
        int b = 1;
        int c ;

            System.out.print(a+","+b);

            for(int i=2; i<num; i++)
            {
                c = a+b;
                System.out.print(","+c);
                a=b;
                b=c;
            }
    }

    // using recursion
    static void fibonacci(int n)
    {
         int a=0, b=1, c =0;

         if(n < 0)
         {
             c = a+b;
             a=b;
             b=c;
             System.out.println(" "+c);
            fibonacci(n-1);
         }
    }

    public static void main(String[] args)
    {
        System.out.println("\n FiboNacci Series program,");
        fiboNacci$eries(20);
    }
}
