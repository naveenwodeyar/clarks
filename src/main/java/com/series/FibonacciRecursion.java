package com.series;

public class FibonacciRecursion
{
    static int fibonacciSeries(int n)
    {
        if(n<1)
            return n;

        return fibonacciSeries(n-1)+fibonacciSeries(n-2);
    }
    public static void main(String[] args)
    {
        System.out.println("\n Fibonacci Series Generator,\n");
        System.out.println(fibonacciSeries(10));
    }
}
