package com.clarks;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ClarksApplication
{

	public static void main(String[] args)
	{
		SpringApplication.run(ClarksApplication.class, args);
		System.out.println("Welcome!");
	}

}
