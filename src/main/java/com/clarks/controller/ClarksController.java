package com.clarks.controller;

import org.springframework.web.bind.annotation.*;

@RestController
public class ClarksController
{
    @GetMapping("/msg")
    public String greetMsg()
    {
        return "Welcome! to the CLARKS App have a great day.";
    }

    @GetMapping("age/{age}")
    public int getYourAge(@PathVariable  int age)
    {
        return age;
    }
}
