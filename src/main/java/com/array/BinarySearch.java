package com.array;

import java.util.Arrays;
import java.util.Scanner;

public class BinarySearch
{
    static void binary2Search(int[] arr)
    {
        Scanner sc = new Scanner(System.in);

        System.out.println("\n Original Array"+ Arrays.toString(arr));
        System.out.println("\n Enter the element to search in the given array:");
            int key=sc.nextInt();

        int result = Arrays.binarySearch(arr,key);

            if(result < 0)
                System.out.println("\n Element not found in the array!");
            else
                System.out.println("\n Element found at the index:"+result);
    }

    public static void main(String[] args)
    {
        System.out.println("\n Java program for Binary search in Array");
        binary2Search(new int[]{1,9,4,8,6,2,81});
    }
}
