package com.array;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayBinarySearch
{
    static int binary2Search(int [] arr,int key)
    {
        System.out.println("\n Original array:" + Arrays.toString(arr));

        int left = 0;
        int right = arr.length - 1;

        while (left <= right) {
            int mid = left + (right - left) / 2;

            if (arr[mid] == key)
                return mid; // Element found,

            if (arr[mid] < key)
                left = mid + 1;
            else
                right = mid - 1;

        }
            return -1; // Element not found.
    }

    public static void main(String[] args)
    {
        System.out.println("\n Java program for searching using Binary search in Array");
        var i = binary2Search(new int[]{1, 9, 4, 8, 6, 2, 81}, 65);
            if(i != -1)
                System.out.println("\n Element found at index:"+i);
            else
                System.out.println("\n Element not found");
    }
}
